 linear_extrude(height= 5) {
    difference() {
        xmax = 165;
        ymax = 105;
        hull(){
            translate([1.5,1.5])
            circle (1.5, $fn=128);
            translate([xmax - 1.5, 1.5])
            circle (1.5, $fn=128);
            translate([1.5, ymax-1.5])
            circle(1.5, $fn=128);
            translate([xmax - 1.5, ymax - 1.5])
            circle(1.5, $fn=128);
        }
        edge = 7;  // inside edge of corner holes
        standoff = 3;
        tap = 1.25;
        holder = 34.5;
        hole1x = 9.45;
        hole2x = 25.05;
        holey = 8.75;
        delta = 2;  // uneven
        for (y = [edge:22.5:90]) {
            translate([ edge + standoff + 2 + hole1x, y + holey])
            circle(tap, $fn=128);
            translate([ edge + standoff + 2 + hole2x, y + holey])
            circle(tap, $fn=128);
        }
        for (y = [(edge+delta+3):22.5:100]) {
            translate ([ edge + standoff + 2 + holder + 15.125 + hole1x, y + holey])
            circle (tap, $fn=128);
            translate ([ edge + standoff + 2 + holder + 15.125 + hole2x, y + holey])
            circle (tap, $fn=128);
        }
        d = 3.5;
        hull() {
            translate([115.25 + 2.5, ymax - edge - 4 - d])
            circle (d, $fn=128);
            translate([115.25 + 2.5, edge + 4 + delta + d])
            circle (d, $fn=128);
        }
        hull () {
            translate ([152 + 2.5, ymax - edge - 4 - d])
            circle(d, $fn=128);
            translate ([152 + 2.5, edge + 4 + delta + d])
            circle(d, $fn=128);
            
        }
        
        translate ([2.5 + 3, 2.5 + 3])
        circle (tap, $fn=128);
        translate ([2.5 + 3, 105 - (2.5 + 3)])
        circle (tap, $fn=128);
        translate ([ 165 - (2.5 + 3), 2.5 + 3])
        circle (tap, $fn=128);
        translate ([165 - (2.5 + 3), 105 - (2.5 + 3)])
        circle (tap, $fn=128);
    }
}